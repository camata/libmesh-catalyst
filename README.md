**libMesh Paraview Catalyst Adaptor **

This adaptor integrates libMesh and ParaView Catalyst to perform in situ data analysis 
and visualization.


_Pre-requisites: _
HDF5 + ZFP:
To Install, Please follow the steps described here:  https://h5z-zfp.readthedocs.io/en/latest/installation.html

_Paraview Catalyst_
1. Download ParaView sources files from ParaView web site.
2. Build using cmake command:

```
   cmake <ParaView Source Dir> -DPARAVIEW_BUILD_EDITION=Calalyst -DPARAVIEW_USE_PYTHON=1 -DPARAVIEW_USE_MPI=1 
   make
   make install 
```



Mode details: Please see https://www.paraview.org/in-situ/

_libMesh (v. 1.5.1)_

1. libMesh uses petsc library, please see https://www.mcs.anl.gov/petsc/documentation/installation.html for details
```
export PETSC_DIR=<petsc install dir>
```
2. Configuration 
```
./configure --prefix=<libmesh install dir> -enable-parmesh HDF5_DIR=<HDF5 install dir> --enable-hdf5
make
make install 
```
-----

**How to use:**
1. Calling CMake:1.a: 
- Create a build directory

- 1.b: Into build dir, call:

```
cmake ${SOURCE_DIR} -DLIBMESH_DIR=<libmesh install dir>  \ 
                    -DParaView_DIR=<Paraview-Catalist-Intall-dir>/lib/cmake/paraview-<Paraview-Version>/ \
                    -DZFP_ROOT_DIR=<zfp install dir> \
                    -DH5Z_ZFP_PLUGIN_DIR=<H5Z ZFP install dir> \
```
2. Call Makefile
```
make 
```

# Check if env var HDF5_PLUGIN_PATH is set up
export HDF5_PLUGIN_PATH=<H5Z_ZFP_PLUGIN_DIR>/plugin








