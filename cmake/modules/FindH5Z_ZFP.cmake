# Standard CMake modules see CMAKE_ROOT/Modules
include(FindPackageHandleStandardArgs)

if(NOT H5Z_ZFP_FOUND)

  if((NOT H5Z_ZFP_PLUGIN_DIR) AND (NOT (ENV{H5Z_ZFP_PLUGIN_DIR} STREQUAL "")))
    set(H5Z_ZFP_PLUGIN_DIR "$ENV{ZFP_ROOT_DIR}")
  endif()

  if(H5Z_ZFP_PLUGIN_DIR)
    set(H5Z_ZFP_INCLUDE_OPTS HINTS ${H5Z_ZFP_PLUGIN_DIR}/include NO_DEFAULT_PATHS)
    set(H5Z_ZFP_LIBRARY_OPTS
      HINTS ${H5Z_ZFP_PLUGIN_DIR}/plugin 
      NO_DEFAULT_PATHS
    )
  endif()

  find_path(H5Z_ZFP_INCLUDE_DIR H5Zzfp_plugin.h ${H5Z_ZFP_INCLUDE_OPTS})
  find_library(H5Z_ZFP_LIBRARY h5zzfp ${H5Z_ZFP_LIBRARY_OPTS})

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(H5Z_ZFP
    FOUND_VAR H5Z_ZFP_FOUND
    REQUIRED_VARS H5Z_ZFP_LIBRARY H5Z_ZFP_INCLUDE_DIR
  )

endif()
